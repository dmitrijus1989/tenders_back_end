<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Entity\Tenders;
use App\Form\TendersType;


/**
 * @Route("/api")
 */
class TendersController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/tenders/get_tenders", name="get_tender")
     */
    public function GetTenders()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $tenders = $entityManager->getRepository(Tenders::class)->findAll([], ['id' => 'DESC']);
        

        if (!$tenders) {
            throw new HttpException(400, "Invalid data");
        }
        
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        
        return $this->json($tenders);
    }

    /**
     * @Rest\Get("/tenders/get_tender/{id}", name="get_tenders")
     */
    public function GetTender(int $id)
    {
        if (!$id) {
            throw new HttpException(400, "Invalid id");
        }

        $entityManager = $this->getDoctrine()->getManager();
        $tender = $entityManager->getRepository(Tenders::class)->find($id);

        if (!$tender) {
            throw $this->createNotFoundException('No product found for id '.$id );
        }
        
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        return $this->json($tender);
    }

    /**
     * @Rest\Post("/tenders/post_tender", name="post_tender")
     */
    public function PostTender(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );
        
        $form = $this->createForm(TendersType::class, new Tenders());
        $form->submit($data);

        if (false === $form->isValid()) {
            return new JsonResponse(
                [
                    'status' => 'error',
                    'errors' => $this->formErrorSerializer->convertFormToArray($form),
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($form->getData());
        $entityManager->flush();

        return new JsonResponse(
            [
                'status' => 'ok',
            ],
            JsonResponse::HTTP_CREATED
        );
        
    }

    /**
     * @Rest\Put("/tenders/put_tender/{id}", name="put_tender")
     */
    public function PutTender(Request $request)
    {
//        $data = json_decode(
//            $request->getContent(),
//            true
//        );
//        
//        $entityManager = $this->getDoctrine()->getManager();
//        
//        $tender = $entityManager->getRepository(Tenders::class)->find($data['id']);
//        $form = $this->createForm(TendersType::class, $tender);
//        $form->submit($data);
//
//        if (false === $form->isValid()) {
//            return new JsonResponse(
//                [
//                    'status' => 'error',
//                    'errors' => $this->formErrorSerializer->convertFormToArray($form),
//                ],
//                JsonResponse::HTTP_BAD_REQUEST
//            );
//        }
//
//        $entityManager->persist($form->getData());
//        $entityManager->flush();
//
//        return new JsonResponse(
//            [
//                'status' => 'ok',
//            ],
//            JsonResponse::HTTP_UPDATED
//        );
        
        
        
        $data = json_decode(
            $request->getContent(),
            true
        );
        
        $entityManager = $this->getDoctrine()->getManager();
        
        $tender = $entityManager->getRepository(Tenders::class)->find($data['id']);
               
        if (!$tender) {
            throw $this->createNotFoundException(sprintf(
                'No tenders found with id "%s"',
                $data['id']
            ));
        }
        
        $form = $this->createForm(TendersType::class, $tender, ['method' => 'PUT']);
        $form->submit($data);
        
        
        
        $entityManager->persist($form->getData());
        $entityManager->flush();
        
        return new JsonResponse(
            [
                'status' => 'ok',
            ],
            JsonResponse::HTTP_OK
        );
        
        
    }

    /**
     * @Rest\Delete("/tenders/delete_tender/{id}", name="delete_tenders")
     */
    public function DeleTetender(int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $tender = $entityManager->getRepository(Tenders::class)->find($id);
        $entityManager->remove($tender);
        $entityManager->flush();

        if (!$tender) {
            throw $this->createNotFoundException('No product found for id '.$id );
        }
        
        return $this->json([
            'status' => 'OK',
            'id' => $tender->getId()
        ]);
    }
}
