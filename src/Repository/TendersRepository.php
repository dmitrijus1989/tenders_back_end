<?php

namespace App\Repository;

use App\Entity\Tenders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tenders|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tenders|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tenders[]    findAll()
 * @method Tenders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TendersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tenders::class);
    }

    // /**
    //  * @return Tenders[] Returns an array of Tenders objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tenders
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
