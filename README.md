# Symfony 5 Tenders REST API

## [DEMO](http://dc.demopuslapis.online/api/tenders/get_tenders)

## Install with Composer
```
    $ curl -s http://getcomposer.org/installer | php
    $ php composer.phar install or composer install
```

## Setting Environment
Change this row in .env file to your DB
```
    $ DATABASE_URL=mysql://carolina_dc:password@localhost/carolina_dc
```

## Getting with Curl
```
    $ curl -H 'content-type: application/json' -v -X GET http://127.0.0.1:8000/api/tenders/get_tenders
    $ curl -H 'content-type: application/json' -v -X GET http://127.0.0.1:8000/api/api/tenders/get_tender/:id
    $ curl -H 'content-type: application/json' -v -X POST -d '{"title":"Foo","description":"Bar"}' http://127.0.0.1:8000/api/tenders/post_tender
    $ curl -H 'content-type: application/json' -v -X PUT -d '{"title":"Foo bar","price":"19.99"}' http://127.0.0.1:8000/api/tenders/put_tender/:id
    $ curl -H 'content-type: application/json' -v -X DELETE http://127.0.0.1:8000/api/tenders/delete_tender/:id
```
